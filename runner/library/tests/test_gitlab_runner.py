# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import json
import os
import sys
import subprocess

from ansible.module_utils import basic
from gitlab_runner import GitLabRunnerModule


@pytest.fixture(scope='session')
def gitlab_registration_token():
    return os.environ.get('GITLAB_REGISTRATION_TOKEN', None)


@pytest.fixture(scope='session')
def package_installed():
    try:
        subprocess.check_output(['gitlab-runner', '--help'],
                                stderr=subprocess.STDOUT)
        return True
    except OSError:
        return False


@pytest.fixture(scope='session')
def performs_registration(package_installed, gitlab_registration_token):
    return package_installed and gitlab_registration_token is not None


@pytest.fixture(scope='session')
def check_mode(package_installed, gitlab_registration_token):
    return not package_installed or gitlab_registration_token is None


def assert_config(must_haves):
    config = None

    with open(os.path.expanduser('~/.gitlab-runner/config.toml'), 'r') as fp:
        config = fp.read()

    for item in must_haves:
        assert item in config


@pytest.mark.parametrize('state', ['present', 'absent'])
def test_gitlab_runner_module(state, gitlab_registration_token,
                              check_mode, performs_registration):

    name = 'tox-spawned-runner'

    data = {
        'ANSIBLE_MODULE_ARGS': {
            'parameters': {
                'name': name,
                'registration-token': gitlab_registration_token,
            },
            'state': state,
            '_ansible_check_mode': check_mode,
        },
    }

    orig = sys.argv
    sys.argv = [
        'gitlab_runner',
        json.dumps(data),
    ]

    with pytest.raises(SystemExit) as exc:
        # Override caching _ANSIBLE_ARGS
        basic._ANSIBLE_ARGS = None
        GitLabRunnerModule()()
    assert exc.value.code == 0

    if performs_registration and state is 'present':
        assert_config(['shell', name])

    sys.argv = orig


@pytest.mark.parametrize('state', ['present', 'absent'])
def test_regression_repeated_parameters(state, gitlab_registration_token,
                                        check_mode, performs_registration):

    name = 'tox-spawned-docker-runner'

    data = {
        'ANSIBLE_MODULE_ARGS': {
            'parameters': {
                'name': name,
                'registration-token': gitlab_registration_token,
                'executor': 'docker',
                'extra': {
                    'docker-volumes': ['a:/opt/a', 'b:/opt/b'],
                    'docker-image': 'debian:buster-slim',
                },
            },
            'state': state,
            '_ansible_check_mode': check_mode,
        },
    }

    orig = sys.argv
    sys.argv = [
        'gitlab_runner',
        json.dumps(data),
    ]

    with pytest.raises(SystemExit) as exc:
        # Override caching _ANSIBLE_ARGS
        basic._ANSIBLE_ARGS = None
        GitLabRunnerModule()()
    assert exc.value.code == 0

    if performs_registration and state is 'present':
        assert_config(['docker', name, 'a:/opt/a', 'b:/opt/b'])

    sys.argv = orig
