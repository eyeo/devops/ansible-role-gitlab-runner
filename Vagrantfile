# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://www.vagrantup.com/docs/vagrantfile/vagrant_version.html
Vagrant.require_version '>= 2.0.0'

# https://www.vagrantup.com/docs/vagrantfile/version.html
Vagrant.configure(2) do |vagrant|

  # https://www.vagrantup.com/docs/synced-folders/basic_usage.html#disabling
  vagrant.vm.synced_folder('.', '/vagrant', disabled: true)

  # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
  vagrant.vm.box_check_update = false
  vagrant.vm.post_up_message = nil

  # https://www.vagrantup.com/docs/multi-machine/
  # https://wiki.debian.org/DebianStretch
  vagrant.vm.define('debian-stretch') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'debian/stretch64'
    config.vm.hostname = 'stretch.test'

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = '2.0'
      ansible.playbook = File.join(__dir__, 'provision-gitlab-runners.yml')
      ansible.extra_vars = {
        'gitlab-runners' => [],
      }
    end

  end

  # https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7.1810
  vagrant.vm.define('centos-7') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'centos/7'
    config.vm.hostname = 'centos7.test'

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = '2.0'
      ansible.playbook = File.join(__dir__, 'provision-gitlab-runners.yml')
      ansible.extra_vars = {
      }
    end

  end

end
